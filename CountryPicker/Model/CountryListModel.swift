//
//  CountryListModel.swift
//  CountryPicker
//
//  Created by webcastle on 29/05/22.
//

import Foundation

public struct CountryListM {
    public  var countryName: String?
    public var capital: String?
}
