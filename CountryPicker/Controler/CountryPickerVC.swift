//
//  CountryPickerVC.swift
//  CountryPicker
//
//  Created by webcastle on 29/05/22.
//

import UIKit

public class CountryPickerVC: UIViewController {

    @IBOutlet public weak var tblview : UITableView!
    
    weak public var delegate: CountryPickerViewDelegate?
    public var arrCountry = [CountryListM]()
    let cellIdentifier  : String = "CountryPickerCell"
    
    public override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setUpDataSource()
        self.tblview.reloadData()
    }
    
    //    Mark: - Custom funcitons

    public func setUpDataSource() {
        self.arrCountry = [CountryListM]()
        arrCountry = [
            CountryListM(countryName: "China", capital: "Beijing"),
            CountryListM(countryName: "India",capital: "New Delhi"),
            CountryListM(countryName: "Indonesia",capital: "Jakarta"),
            CountryListM(countryName: "Pakistan",capital: "Islamabad"),
            CountryListM(countryName: "Bangladesh",capital: "Dhaka"),
            CountryListM(countryName: "Japan",capital: "Tokyo"),
            CountryListM(countryName: "Philippines",capital: "Manila"),
            CountryListM(countryName: "Vietnam",capital: "Hanoi"),
            CountryListM(countryName: "Turkey",capital: "Ankara"),
            CountryListM(countryName: "Iran",capital: "Tehran"),
            CountryListM(countryName: "Thailand",capital: "Bangkok"),
            CountryListM(countryName: "Myanmar",capital: "Nay Pyi Taw"),
            CountryListM(countryName: "South Korea",capital: "Seoul"),
            CountryListM(countryName: "Iraq",capital: "Baghdad"),
            CountryListM(countryName: "Afghanistan",capital: "Kabul")
        ]
        
    }
    
   

}

//    Mark: - UITableViewDelegate and UITableViewDataSource

extension CountryPickerVC: UITableViewDataSource, UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrCountry.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        
        if nil == cell
        {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        }
        
        cell?.textLabel?.text = arrCountry[indexPath.row].countryName
        cell?.textLabel?.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        return cell!
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
        self.dismiss(animated: false) {
            self.delegate?.countryPickerView(self, didSelectcountry: self.arrCountry[indexPath.row].capital ?? "")
        }
    }
    
}
