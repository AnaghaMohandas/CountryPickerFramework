//
//  CountryPickerProtocol.swift
//  CountryPicker
//
//  Created by webcastle on 29/05/22.
//

import Foundation

//    Mark: - Custom protocol

public protocol CountryPickerViewDelegate: class {
    
    // Called when user selects a country
    func countryPickerView(_ countryPickerView: CountryPickerVC,didSelectcountry country: String)
    
}
